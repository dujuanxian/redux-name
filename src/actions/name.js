export const SET_NAME = 'SET_NAME';
export const GET_NAME = 'GET_NAME';

function setName(name) {
  return {
    type: SET_NAME,
    payload: name
  };
}

function getName() {
  return (dispatch) => {
    fetch("https://jsonplaceholder.typicode.com/users/1")
      .then(response => response.json())
      .then(result => {
        dispatch({
          type: GET_NAME,
          payload: result.name
        })
      })
  };
}

export {
  setName,
  getName
}